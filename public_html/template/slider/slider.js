'use strict';
    var multiItemSlider = (function () {
      function _isElementVisible(element) {
        var rect = element.getBoundingClientRect(),
          vWidth = window.innerWidth || doc.documentElement.clientWidth,
          vHeight = window.innerHeight || doc.documentElement.clientHeight,
          elemFromPoint = function (x, y) { return document.elementFromPoint(x, y) };

        // Return false if it's not in the viewport
        if (rect.right < 0 || rect.bottom < 0
          || rect.left > vWidth || rect.top > vHeight)
          return false;

        // Return true if any of its four corners are visible
        return (
          element.contains(elemFromPoint(rect.left, rect.top))
          || element.contains(elemFromPoint(rect.right, rect.top))
          || element.contains(elemFromPoint(rect.right, rect.bottom))
          || element.contains(elemFromPoint(rect.left, rect.bottom))
        );
      }
      return function (selector, config) {
        var _config = {
            slideToShow: 1, // колличество видимых слайдов
            slideToScroll: 1, // шаг переключения слайдов
            speed: 600,
            infinity: false, // бессконечный слайдер
            autoplay: false, // автоматическая смена слайдов
            direction: 'right', // направление смены слайдов
            interval: 5000, // интервал между автоматической сменой слайдов
            pause: true // устанавливать ли паузу при поднесении курсора к слайдеру
          };
         
        for (var key in config) {
          if (key in _config) {
            _config[key] = config[key];
          }
        }
          
        var
          _mainElement = document.querySelector(selector), // основный элемент блока
          _sliderWrapper = _mainElement.querySelector('.slider__wrapper'), // обертка для .slider-item
          _sliderItems = _mainElement.querySelectorAll('.slider__item'), // элементы (.slider-item)
          _sliderControls = _mainElement.querySelectorAll('.slider__control'), // элементы управления
          _sliderControlLeft = _mainElement.querySelector('.slider__control_left'), // кнопка "LEFT"
          _sliderControlRight = _mainElement.querySelector('.slider__control_right'), // кнопка "RIGHT"
          _wrapperWidth = parseFloat(getComputedStyle(_sliderWrapper).width), // ширина обёртки
          _positionLeftItem = 0, // позиция левого активного элемента
          _transform = 0, // значение транфсофрмации .slider_wrapper
          _items = [], // массив элементов
          _interval = 0,
          _slidesWidth = _wrapperWidth/_config.slideToShow + "px", // ширина одного элемента (для отображения)
          _itemWidth = _wrapperWidth/_config.slideToShow, // ширина одного элемента (для работы слайдера)
          _step = _itemWidth / _wrapperWidth * 100 * _config.slideToScroll, // величина шага (для трансформации)
          _scroll = true;
          //Задаем скорость анимации слайдов
          _sliderWrapper.style.transition = "transform "+_config.speed+"ms ease";
        // наполнение массива _items
        _sliderItems.forEach(function (item, index) {
          _items.push({ item: item, position: index, transform: 0});
          item.style.flex="0 0 "+_slidesWidth; // CSS Параметр flex 
          item.style.maxWidth=_slidesWidth; // CSS Параметр max-width
        });
          
         

          var position = {
              getItemMin: function () {
                  var indexItem = 0;
                  _items.forEach(function (item, index) {
                      if (item.position < _items[indexItem].position) {
                          indexItem = index;
                      }
                  });
                  return indexItem;
              },
              getItemMax: function () {
                  var indexItem = 0;
                  _items.forEach(function (item, index) {
                      if (item.position > _items[indexItem].position) {
                          indexItem = index;
                      }
                  });
                  return indexItem;
              },
              getMin: function () {
                  return _items[position.getItemMin()].position;
              },
              getMax: function () {
                  return _items[position.getItemMax()].position;
              }
          }

          var _transformItem = function (direction) {
              var nextItems,
                  stepItem,
                  firstItem,
                  lastItem;

              if (!_isElementVisible(_mainElement)) {
                  return;
              }              

              if (direction === 'right') {
                  _positionLeftItem += _config.slideToScroll;
                  nextItems =_positionLeftItem+_config.slideToShow-1;

                  if((nextItems) > position.getMax()&&_scroll === true) {

                      stepItem = nextItems - position.getMax();
                      for(var i=0; i<=stepItem; i++){
                          firstItem = position.getItemMin();

                          _items[firstItem].position = position.getMax() + 1;
                          _items[firstItem].transform += _items.length * 100;
                          _items[firstItem].item.style.transform = 'translateX(' + _items[firstItem].transform + '%)';
                      }
                  }
                  _transform -= _step;
              }
              if (direction === 'left') {
                  _positionLeftItem -= _config.slideToScroll;

                  if (_positionLeftItem < position.getMin()&&_scroll === true) {
                      stepItem = position.getMin() - _positionLeftItem;
                      for(var i=0; i<=stepItem; i++){
                          firstItem = position.getItemMax();

                          _items[firstItem].position = position.getMin()-1;
                          _items[firstItem].transform -= _items.length * 100;
                          _items[firstItem].item.style.transform = 'translateX(' + _items[firstItem].transform + '%)';
                      }
                  }
                  _transform += _step;
              }
              _sliderWrapper.style.transform = 'translateX(' + _transform + '%)';
          }

          var _cycle = function (direction) {
              if (!_config.autoplay) {
                  return;
              }
              _interval = setInterval(function () {
                  _transformItem(direction);
              }, _config.interval);
          }

          // обработчик события click для кнопок "назад" и "вперед"
          var _controlClick = function (e) {
              var direction = this.classList.contains('slider__control_right') ? 'right' : 'left';
              e.preventDefault();
              _transformItem(direction);
              clearInterval(_interval);
              _cycle(_config.direction);
          };

          // обработка события изменения видимости страницы
          var _handleVisibilityChange = function () {
              if (document.visibilityState === "hidden") {
                  clearInterval(_interval);
              } else {
                  clearInterval(_interval);
                  _cycle(_config.direction);
              }
          }

          var _setUpListeners = function () {
              // добавление к кнопкам "назад" и "вперед" обрботчика _controlClick для событя click
              _sliderControls.forEach(function (item) {
                  item.addEventListener('click', _controlClick);
              });
              _mainElement.addEventListener('touchstart', function (e) {
                  _startX = e.changedTouches[0].clientX;
              });
              _mainElement.addEventListener('touchend', function (e) {
                  var
                  _endX = e.changedTouches[0].clientX,
                      _deltaX = _endX - _startX;
                  if (_deltaX > 50) {
                      _transformItem('left');
                  } else if (_deltaX < -50) {
                      _transformItem('right');
                  }
              });
              if (_config.pause && _config.autoplay) {
                  _mainElement.addEventListener('mouseenter', function () {
                      clearInterval(_interval);
                  });
                  _mainElement.addEventListener('mouseleave', function () {
                      clearInterval(_interval);
                      _cycle(_config.direction);
                  });
              }
              document.addEventListener('visibilitychange', _handleVisibilityChange, false);
          }

          // инициализация
          _setUpListeners();
          if (document.visibilityState === "visible") {
              _cycle(_config.direction);
          }

          return {
              right: function () { // метод right
                  _transformItem('right');
              },
              left: function () { // метод left
                  _transformItem('left');
              },
              stop: function () { // метод stop
                  _config.autoplay = false;
                  clearInterval(_interval);
              },
              cycle: function () { // метод cycle 
                  _config.autoplay = true;
                  clearInterval(_interval);
                  _cycle();
              }
          }

      }
    }());
