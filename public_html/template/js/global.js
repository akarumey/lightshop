window.onload = function (){

    if(document.querySelector("input[name='delivery']")){
        var radio = document.querySelectorAll("input[name='delivery']"),
            fild = document.querySelector("input[name='sum']"),
            address = document.querySelector(".address--form"),
            sum = document.querySelector("#data-price").getAttribute("data-price");

        for (check of radio){
            check.onchange = function(){
                var id = this.getAttribute("id"),
                    delivery = this.getAttribute("data-price"),
                    to_pay = parseInt(sum) + parseInt(delivery);
                if(id === "stock"){
                    address.classList.add("hide");
                } else {
                    address.classList.remove("hide");
                }
                fild.setAttribute('value', to_pay);
                document.querySelector("#data-price").innerHTML = to_pay;
            }
        }
    }

    if(document.querySelector(".search__block")){
        var _search__block = document.querySelector(".search__block"),
            _search__input = _search__block.querySelector(".search--string"),
            _search__btn = _search__block.querySelector(".search--btn"),
            _search__content = _search__block.querySelector(".search--content");

        _search__input.oninput = function () {
            var val = this.value.trim();
            var _data = new FormData;
            _data.append('search', val);
            _search__content.innerHTML = '';

            if(val.length > 1){
                _search__content.style.display = "block";
                fetch("/searching/", {
                    method: "POST",
                    body: _data,
                })
                    .then(function (response) {
                        return response.json()
                    })
                    .then(function (data) {

                        for (var content of data ){
                            for (var value of content){
                                if(value.name != undefined){
                                    var name = value.name;
                                }
                                if(value.price != undefined){
                                    var price = value.price;
                                }
                                if(value.short_description != undefined){
                                    var short_description = value.short_description;
                                }
                                if(value.id != undefined){
                                    var id = value.id;
                                }
                            }
                            _search__content.innerHTML +=
                                "<a href='/product/"+id+"'>"+name+"    ///    "
                                +short_description+"    ///    "+price+"Грн</a><br>";
                        }

                    });

            } else {
                _search__content.style.display = "none";
            }
        }
    }

    if(document.querySelector(".drop--menu")){
        var menu = document.querySelectorAll(".drop--menu");

        for (item of menu){
            item.onclick = function(){
                if(this.classList.contains("active")){
                    this.classList.remove("active");
                } else {
                    this.classList.add("active");
                }
            }
        }
    }

    if(document.querySelector("#slider_1")){
        var slider = multiItemSlider('#slider_1', {
            slideToShow: 1,
            autoplay: true,
            interval: 2000,
        });
    }

    if(document.querySelector("#product-slider")){
        var product_slider = multiItemSlider('#product-slider', {
            slideToShow: 5,
        });
    }

    if(document.querySelector(".add-to-cart")) {
        var btns = document.querySelectorAll(".add-to-cart");
        for (btn of btns) {
            btn.onclick = function (e) {
                e.preventDefault();
                var check = document.querySelector(".added");
                var id = this.getAttribute("data-id");
                fetch("/cart/add/"+id)
                    .then(function(response) {
                        return response.text();
                    })
                    .then(function (text) {
                        document.querySelector("#cart-count").innerHTML = text;
                        btn.classList.add("hide");
                        check.classList.remove("hide");
                    });
                if(this.classList.contains("and-bay")){
                    window.location.href = "/cart/"
                }
            }
        }
    }

    if(document.querySelector("#order__form")) {
        var form = document.querySelector("#order__form");

        form.onsubmit = function (e) {
            e.preventDefault();
            var data = new FormData(form);

            fetch("/cart/reg_order",{
                method: "POST",
                body: data,
            })
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    if (data.status == 'success'){
                        document.querySelector(".message").innerHTML = ("<div class='success'>"+data.text+"</div>");
                    } else if (data.status == 'link'){
                        window.location.href=data.text;
                    } else if (data.status == 'error'){
                        document.querySelector(".message").innerHTML = "<div class='error'>"+data.text+"</div>";
                    }
                });

        }
    }
};
/*
if (data.status == 'success'){
    $('.message').html("<div class='success'>"+data.text+"</div>");
    $('.message').show();
    setTimeout(function(){$('.message').slideUp(3000);}, 1000);
} else if (data.status == 'link'){
    window.location.href=data.text;
} else if (data.status == 'error')  {
    $('.message').html("<div class='error'>"+data.text+"</div>");
    $('.message').show();
    setTimeout(function(){$('.message').slideUp(3000);}, 1000);
}*/
