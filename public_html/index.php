<?php
session_start();
include __DIR__ . "/../classes/load.php";

//Подключаем исспользуемые классы
use classes\core\Router;

//Общие настройки
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

//Вызов Роутер
$route = new Router;
$route->run();

function debag($value, $type){
    echo "</pre>";
    if ($type == 'echo' || $type == 'e'){
        echo $value;
    } else {
        var_dump($value);
    }
    echo "</pre>";
}
