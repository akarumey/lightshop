<?php
return [
    //Localhost
    'db' => [
        'host' => 'localhost',
        'port' => '3306',
        'username' => 'lightshop',
        'password' => 'lightshop',
        'database' => 'lightshop',
        'charset' => 'utf8',
    ],
    //WebHost
    /*'db' => [
        'host' => 'localhost',
        'port' => '3306',
        'username' => 'id9650045_lightshop',
        'password' => 'lightshop',
        'database' => 'id9650045_lightshop',
        'charset' => 'utf8',
    ],*/
    'routes' =>[
        'cart/([a-z]+)/([0-9]+)' => 'cart/$1/$2',
        'cart/([a-z]+)' => 'cart/$1',
        'cart' => 'cart/index',
        'catalog/discount' => 'catalog/discount',
        'catalog/([a-z]+)/page:([0-9]+)' => 'catalog/all/$2/$1',
        'catalog/page:([0-9]+)' => 'catalog/all/$1/$2',
        'admin/([a-z]+)/item/([a-z]+)' => 'admin/create/$1/$2',
        'admin/([a-z]+)/item/([0-9]+)' => 'admin/item/$1/$2',
        'admin/([a-z]+)/page:([0-9]+)' => 'admin/views/$1/$2',
        'admin/([a-z]+)' => 'admin/$1',
        'admin' => 'admin/index',
        '([a-z]+)'=>'main/$1',
        ''=>'main/index',

    ],
];
