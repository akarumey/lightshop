<?php
    $okpu = "37733537";
    $account = "26002050236308";
    $bank = "ПАТ КБ «ПРИВАТБАНК»";
    $mfo = "305299";

?>
<div class="success__container">
    <div class="success__block">
        <div class="success__content">
            <div class="success__header">
                <h3 class="success__title">
                    Заказ <b>№<?=$order['order_number'];?></b> оформлен!
                </h3>

                <span class="success__toPay">
                    К оплате: <b><?=$order['sum'];?> Грн</b>
                </span>
            </div>
            <div class="success__body">
                <table style="width: 100%">
                    <tr>
                        <th>Реквизиты для оплаты:</th>
                    </tr>
                    <tr>
                        <td>
                            <span><b>ЄДРПОУ:</b>&nbsp;<?=$okpu;?></span>
                        </td>
                        <td>
                            <span><b>Р/С:</b>&nbsp;<?=$account;?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><b>Банк:</b>&nbsp;<?=$bank;?></span>
                        </td>
                        <td>
                            <span><b>МФО:</b>&nbsp;<?=$mfo;?></span></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>