<?php
use classes\core\Generator;
$model = new Generator;
?>

<div class='container'>
    <div class="row">
        <form class="sort__form" name="sort_form" action='' method='post''>
        <span>Сортировать:</span>
        <select name="sort" onChange="this.form.submit()">
            <option value=""></option>
            <option <?php if($_SESSION['sort'] == 'sort_up'){echo 'selected';}?> value="sort_up">
                По возростанию
            </option>
            <option <?php if($_SESSION['sort'] == 'sort_down'){echo 'selected';}?> value="sort_down">
                По убыванию
            </option>
        </select>
        <input type="submit" style="display: none;"/>
        </form>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="catalog__nav__container">
                <a href="/catalog/page:1"><h3>Категории</h3></a>
                <?php foreach ($catalog as $key=>$value): ?>
                    <a href="/catalog/<?=$catalog[$key]->alias;?>/page:1"
                        class="nav__item <?php if($category == $catalog[$key]->alias){echo "active";}?>">
                        <?=$catalog[$key]->name;?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-9">
            <?php $model->gridGenerator($grid_catalog);?>
            <div class="row">
                <ul class="pagination__block">
                    <?php if($page != 1):?>
                    <li class="pagination__item">
                        <a class="pagination__link pagination__skip" href="/catalog/<?=$first;?>">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </li>
                    <?php endif;?>

                    <?php if($count != 1):?>
                    <?php foreach ($pagination as $key => $value):?>
                    <li class="pagination__item">
                        <a class="pagination__link" href="/catalog/<?=$value['link'];?>">
                            <?= $value['num'];?>
                        </a>
                    </li>
                    <?php endforeach;?>
                    <?php endif;?>
                    <?php if($page != $count AND $count != 0):?>
                    <li class="pagination__item">
                        <a class="pagination__link pagination__skip" href="/catalog/<?=$last;?>">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>