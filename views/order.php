<div class="container">
    <div class="message"></div>
    <form id="order__form" method="post" action="">
        <div class="row justify-content-center">
            <h2>Оформление заказа</h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div> <h4 style="text-align: center">Контактные данные</h4>
                   <div class="form__input">
                       <span class="form__label">Email</span>
                       <input name="email" type="email">
                   </div>
                   <div class="form__input">
                       <span class="form__label">Телефон</span>
                       <input name="phone" type="text">
                   </div>
                   <div class="form__input">
                       <span class="form__label">Имя</span>
                       <input name="first_name" type="text">
                   </div>
                   <div class="form__input">
                       <span class="form__label">Фамилия</span>
                       <input name="last_name" type="text">
                   </div>
               </div>
            </div>
            <div class="col-md-6">
                <h4 style="text-align: center; width: 100%;">Ваш заказ</h4>
                <table style="width: 100%; text-align: end">
                    <tr>
                        <th class="start">ID</th>
                        <th class="center">Название</th>
                        <th class="center">Количество</th>
                        <th class="end">Цена</th>
                    </tr>
                    <?php foreach ($products as $key => $value):?>

                        <tr>
                            <td class="start"><?=$products[$key]->id;?></td>
                            <td class="center"><?=$products[$key]->name;?></td>
                            <td class="center"><?=$productInCart[$products[$key]->id];?></td>
                            <td class="end"><?=$products[$key]->price;?> Грн</td>
                        </tr>

                    <?php endforeach;?>
                </table>
                <div style="display: flex; justify-content: flex-end; margin-top: 30px; width: 100%">
                    <span><b>Общая сумма:</b> 	&nbsp;	&nbsp;</span><span><?=$sum_to_pay;?> Грн</span>
                </div>
            </div>
        </div>
        <div class="row">
            <h4 style="text-align: center; width: 100%;">Способы доставки</h4>
            <table style="width: 100%; text-align: end">
                <tr>
                    <th></th>
                    <th class="start">Способ доставки</th>
                    <th class="end">Цена</th>
                    <th></th>
                    <th class="start">Способ доставки</th>
                    <th class="end">Цена</th>
                </tr>
                <tr>
                    <td class="center">
                        <input name="delivery" id="stock" value="stock" type="radio" data-price="0" checked>
                    </td>
                    <td class="start">
                        <label class="order__label" for="stock">Самовывоз</label>
                    </td>
                    <td class="end"><span >Бессплатно</span></td>
                    <td class="center">
                        <input name="delivery" id="in-time" value="in-time" type="radio" data-price="20">
                    </td>
                    <td class="start">
                        <label class="order__label" for="in-time">In-time</label>
                    </td>
                    <td class="end">+<span>20</span> Грн</td>
                </tr>
                <tr>
                    <td class="center">
                        <input name="delivery" id="new_post" value="new_post" type="radio" data-price="35">
                    </td>
                    <td class="start">
                        <label class="order__label" for="new_post">Новая почта</label>
                    </td>
                    <td class="end">+<span>35</span> Грн</td>
                    <td class="center">
                        <input name="delivery" id="ukr_post" value="ukr_post" type="radio" data-price="60">
                    </td>
                    <td class="start">
                        <label class="order__label" for="ukr_post">Укр почта</label>
                    </td>
                    <td class="end">+ <span>60</span> Грн</td>
                </tr>
            </table>
        </div>
        <div class="row address--form hide">
            <div class="col-md-12">
                <h4 style="text-align: center">Адресс</h4>
            </div>
            <div class="col-md-6">
                <div class="form__input">
                    <span class="form__label">Город</span>
                    <input name="city" type="text">
                </div>
                <div class="form__input">
                    <span class="form__label">Дом</span>
                    <input name="house" type="text">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form__input">
                    <span class="form__label">Улица</span>
                    <input name="street" type="text">
                </div>
                <div class="form__input">
                    <span class="form__label">Квартира</span>
                    <input name="apartment" type="text">
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div style="display: flex; justify-content: flex-end; margin-top: 30px; width: 100%">
                <input type="hidden" name="sum" value="<?=$sum_to_pay;?>">
                <span style="font-size: 25px"><b>К оплате:</b> 	&nbsp;	&nbsp;</span>
                <span style="font-size: 25px" id="data-price" data-price="<?=$sum_to_pay;?>"><?=$sum_to_pay;?></span>
                <span style="font-size: 25px">&nbsp;Грн</span>
            </div>
            <input class="BTN added" style="margin: 20px 0" type="submit" name="submit" value="Подтвердить">
        </div>
    </form>
</div>