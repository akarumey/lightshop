<div class="row">
    <div class="col-md-12">
        <h4>Заказ №<?=$item->order_number;?></h4>
    </div>
    <div class="col-md-6">
        <span><b>Имя:</b> <?=$item->first_name;?> <?=$item->last_name;?></span><br>
        <span><b>Email:</b> <?=$item->email;?></span><br>
        <span><b>Номер телефона:</b> <?=$item->phone;?></span><br>
        <?php if(!empty($item->address)){
            echo "<span><b>Адресс:</b>". $item->address ."</span><br>";
        }?>
        <span><b>Доставка:</b> <?=$item->delivery;?></span><br>
    </div>
    <div class="col-md-6">
        <table width="100%">
            <tr>
                <th> </th>
                <th>ID:</th>
                <th>Название</th>
                <th>Цена</th>
            </tr>
            <?php foreach ($productInCart as $product):?>
                <tr>
                    <td><?=$count[$product->id]?>x</td>
                    <td><?=$product->id?></td>
                    <td><?=$product->name?></td>
                    <td><?=$product->price?> Грн</td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
    <div class="col-md-12" style="margin-top: 20px">
       <h5><b><span>К оплате: <?=$item->sum;?> Грн</span></b></h5>
    </div>
</div>


