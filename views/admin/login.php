<html>
    <head>
        <meta charset="utf-8">
        <title>Admin Login</title>

        <!--Favicon-->
        <link rel="shortcut icon" type="image/png" href="/public_html/images/admin_favicon.png"  >

        <!--JQuery-->
        <script src="/template/js/jquery-3.3.1.min.js"></script>

        <!--Bootstrap-->
        <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css">

        <!--My CSS-->
        <link rel="stylesheet" href="/template/css/login.css">

        <!--My JS-->
        <script src="/template/js/global.js"></script>
    </head>
    <body>
    <div class='__message'></div>
        <div class='container login__container'>
            <div class='row login__wrapper'>
                <form class='login__form' enctype='multipart/form-data' method='post' action='' >

                    <h3 class='login__title'>Авторизация</h3>
                    <div class='login__block'>
                        <label class="login__label">Логин</label><br>
                        <input class='login__input' type="text" name="login">
                    </div>
                    <div class='login__block'>
                        <label class="login__label">Пароль</label><br>
                        <input class='login__input' type="password" name="password">
                    </div>
                    <input type='hidden' name='action' value='login'>
                    <input class="login__input login__submit btn--ajax" type="submit" name='submit' value="Войти">
                </form>
            </div>
        </div>
    </body>
</html>