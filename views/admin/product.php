<div><h4>Товары</h4><a href="/admin/product/item/new">Создать новый товар</a></div>
<table width="100%" border="1">
    <tbody>
    <tr>
        <th>ID</th>
        <th>Category</th>
        <th>Name</th>
        <th>Price</th>
        <th>Short_description</th>
        <th>Count</th>
        <th>Discount</th>
        <th>Status</th>
        <th class="center">Действия</th>
    </tr>
    <?php foreach ($items as $value):?>
        <tr>
            <td><?=$value->id;?></td>
            <td><?=$value->category;?></td>
            <td><?=$value->name;?></td>
            <td><?=$value->price;?>грн</td>
            <td><?=$value->short_description;?></td>
            <td><?=$value->count;?>шт.</td>
            <td><?=$value->discount;?>%</td>
            <td><?=$value->status;?></td>
            <td class="center">
                <a href="/admin/product/item/<?=$value->id;?>">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
<div class="row">
    <ul class="pagination__block">
        <?php if($page != 1):?>
            <li class="pagination__item">
                <a class="pagination__link pagination__skip" href="/admin/<?=$param?>/<?=$first;?>">
                    <i class="fas fa-chevron-left"></i>
                </a>
            </li>
        <?php endif;?>

        <?php if($count != 1):?>
            <?php foreach ($pagination as $key => $value):?>
                <li class="pagination__item">
                    <a class="pagination__link" href="/admin/<?=$param?>/<?=$value['link'];?>">
                        <?= $value['num'];?>
                    </a>
                </li>
            <?php endforeach;?>
        <?php endif;?>
        <?php if($page != $count AND $count != 0):?>
            <li class="pagination__item">
                <a class="pagination__link pagination__skip" href="/admin/<?=$param?>/<?=$last;?>">
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
        <?php endif;?>
    </ul>
</div>