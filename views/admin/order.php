<div><h4>Заказы</h4></div>
<table width="100%" border="1">
    <tbody>
    <tr>
        <th>Номер заказа</th>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Доставка</th>
        <th>Сумма</th>
        <th class="center">Действия</th>
    </tr>
    <?php foreach ($items as $value):?>
        <tr>
            <td><?=$value->order_number;?></td>
            <td><?=$value->first_name;?> <?=$value->last_name;?></td>
            <td><?=$value->phone;?></td>
            <td><?=$value->delivery;?></td>
            <td><?=$value->sum;?> Грн</td>
            <td class="center">
                <a href="/admin/order/item/<?=$value->id;?>">
                    <i class="fas fa-info-circle"></i>
                </a>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
<div class="row">
    <ul class="pagination__block">
        <?php if($page != 1):?>
            <li class="pagination__item">
                <a class="pagination__link pagination__skip" href="/admin/<?=$param?>/<?=$first;?>">
                    <i class="fas fa-chevron-left"></i>
                </a>
            </li>
        <?php endif;?>

        <?php if($count != 1):?>
            <?php foreach ($pagination as $key => $value):?>
                <li class="pagination__item">
                    <a class="pagination__link" href="/admin/<?=$param?>/<?=$value['link'];?>">
                        <?= $value['num'];?>
                    </a>
                </li>
            <?php endforeach;?>
        <?php endif;?>
        <?php if($page != $count AND $count != 0):?>
            <li class="pagination__item">
                <a class="pagination__link pagination__skip" href="/admin/<?=$param?>/<?=$last;?>">
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
        <?php endif;?>
    </ul>
</div>