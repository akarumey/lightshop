<?php
session_start();
if(!$_SESSION['admin_id']) {header("Location: /admin/login");}
?>
<html>
<head>
    <meta charset="utf-8">
    <title>Lightshop</title>

    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="/public_html/images/admin_favicon.png">

    <!--JQuery-->
    <script src="/template/js/jquery.min.js"></script>

    <!--Fontawesome-->
    <link rel="stylesheet" href="/template/fontawesome/css/all.css">
    <script src="/template/fontawesome/js/all.js"></script>

    <!--Bootstrap-->
    <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css">

    <!--My CSS-->
    <link rel="stylesheet" href="/template/css/main.css">

    <!--My JS-->
    <script src="/template/js/global.js"></script>
</head>
<body>
<?php
$admin__background = false;
?>
<?php if($admin__background):?>
    <span class="admin__background" style="
        background-image: url('https://realadmin.ru/assets/images/articles/2017/12/1.jpg');
        background-size: cover;
        background-position: center;">
    </span>
    <span class="admin__background" style="background-color: rgba(255, 255, 255, 0.61);"></span>
<?php endif;?>
<header>
    <div class="container-fluid header__content__container">
        <div class="container">
            <div class="row header__content">
                <div class="header__logo">
                    <a href="/">LIGHTSHOP</a>
                </div>
                <div class="header__logout">
                    <a href="/admin/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="container-fluid content__container" style="position: relative">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="/admin/order/page:1">Заказы</a>
                <br>
                <br>
                <h5>Магазин</h5>
                <div><a href="/admin/category/page:1">Категории</a></div>
                <div><a href="/admin/product/page:1">Товары</a></div>
            </div>
            <div class="col-md-10">
                <?php require_once $content;?>
            </div>
        </div>
    </div>

</section>
<footer class="container-fluid footer__container" style="z-index: 5">
    <div class="container">
        <div class="row">
            <div class="footer__copy">
                &copy; Lightshop 2019
            </div>
        </div>
    </div>
</footer>
</body>
</html>