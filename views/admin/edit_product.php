<form name="product" action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">

            <div class="edit__content__item">ID: <?=$item->id?></div>

            <div class="edit__content__item">
                <div>Категория: </div>
                <div>

                    <select name="category">

                        <option value=""></option>

                        <?php foreach($categoryes as $category):?>

                            <option value="<?=$category->alias?>" <?php if($category->alias == $item->category){echo "selected";}?>>
                                <?=$category->name?>
                            </option>

                        <?php endforeach;?>

                    </select>

                </div>
            </div>

            <div class="edit__content__item">
                <div>Название: </div>
                <div>
                    <input name="name" type="text" value="<?=$item->name?>">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Картинка: </div>
                <div>
                    <input type="file" name="image">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Цена: </div>
                <div>
                    <input name="price" type="number" value="<?=$item->price?>">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Краткое описание: </div>
                <div>
                    <input name="short_description" type="text" value="<?=$item->short_description?>">
                </div>
            </div>

            <div>
                <div>Описание: </div>
                <div class="edit__content__item">
                    <textarea style="width: 100%; min-height: 100px;" name="description"><?=$item->description?></textarea>
                </div>
            </div>

            <div class="edit__content__item">
                <div>Количество: </div>
                <div>
                    <input name="count" type="number" value="<?=$item->count?>">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Скидка: </div>
                <div>
                    <input name="discount_status" type="text" value="<?=$item->discount_status?>">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Процент скидки: </div>
                <div>
                    <input name="discount" type="number" value="<?=$item->discount?>">
                </div>
            </div>

            <div class="edit__content__item">
                <div>Есть в наличии(1-да / 0-нет): </div>
                <div>
                    <input name="status" type="number" value="<?=$item->status?>">
                </div>
            </div>
            <input type="hidden" name="table" value="product">
            <div class="edit__content__btn">
                <input class="BTN added" name="submit" type="submit" value="Сохранить">
            </div>
        </div>
        <div class="col-md-6">
            <img style="width: 100%" src="/images/<?=$item->image?>" alt="<?=$item->image?>">
        </div>
    </div>
</form>