<?php
use classes\core\Generator;
$model = new Generator;
?>

<div class="slider" id="slider_1" style="height: 400px; margin: 0 -15px">
    <div class="slider__wrapper">
        <?php foreach ($slides as $slide):?>
        <?php if($slide['status'] == 1):?>
        <div class="main-slider-slide slider__item" style="position: relative; height: 400px; background-size: cover; background-image: url('<?=$slide['bg'];?>');">
            <span style="position: absolute; top: 0; background-color: #000; opacity: 0.6; height: 100%; width: 100%; display: block;"></span>
            <div class="container main-slider-container">
                <div class="main-slider-content">
                    <div class="row" style="height: 100%; align-items: center">
                        <div class="col-md-8">
                            <div style="margin-bottom: 40px">
                                <h2 style="color: white;"><?=$slide['title'];?></h2>
                            </div>
                            <div>
                                <p style="color: white; font-size: 25px"><?=$slide['text'];?></p>
                            </div>
                            <a href="<?=$slide['btn_link'];?>" class="BTN slider--btn">
                                <span><?=$slide['btn_text'];?></span>
                            </a>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php endforeach;?>
    </div>
    <!-- КНОПКИ "НАЗАД" И "ВПЕРЁД" -->
    <a class="slider__control slider__control_left slider__control_show" href="#" role="button"></a>
    <a class="slider__control slider__control_right slider__control_show" href="#" role="button"></a>
</div>
<div class="container">
    <?php $model->gridGenerator($grid_discount);?>
</div>
<div class="container">
    <?php $model->gridGenerator($grid_new);?>
</div>
