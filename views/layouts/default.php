<?php
use classes\shop\Category;
use classes\core\Cart;

$shop_phone = '(050)-972-33-79'; // (XXX)-XXX-XX-XX
$shop_email = 'info@lightshop.ml'; // example@example.com
$shop_telegram = 'Alex_Savchuk_08'; // login
//$shop_instagram = ' '; // login
$shop_viber='380509723379'; // XXXXXXXXXXXX
$shop_whatsapp='0509723379'; // XXXXXXXXXX

$model = new Category;
$catalog = $model::find()->all();
?>
<html>
<head>
    <meta charset="utf-8">
    <title>Lightshop</title>

    <!--Favicon-->
    <link rel="shortcut icon" type="image/x-icon" href="/public_html/images/shop_favicon.png">

    <!--Fontawesome-->
    <link rel="stylesheet" href="/template/fontawesome/css/all.css">
    <script src="/template/fontawesome/js/all.js"></script>

    <!--Slider-->
    <link rel="stylesheet" type="text/css" href="/template/slider/slider.css"/>

    <!--Bootstrap-->
    <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css">

    <!--My CSS-->
    <link rel="stylesheet" href="/template/css/main.css">

    <!--My JS-->
    <script src="/template/js/global.js"></script>
</head>
<body>
<header>
    <div class="container-fluid header__info__container">
        <div class="container">
            <div class="row header__info">
                <div class="info__block info__callback">
                    <?php if(isset($shop_phone)): ?>
                        <a class="callback__item info__callback--phone" href="tel:<?=$shop_phone?>">
                            <i class="fas fa-phone"></i><?=$shop_phone?>
                        </a>
                    <?php endif;  if(isset($shop_email)): ?>
                        <a class="callback__item info__callback--email" href="mailto:<?=$shop_email?>">
                            <i class="fas fa-envelope"></i><?=$shop_email?>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="info__block info__social">
                    <?php if(isset($shop_telegram)): ?>
                        <div class="social__item info__social--telegram">
                            <a href="https://t.me/<?=$shop_telegram?>">
                                <i class="fab fa-telegram"></i>
                            </a>
                        </div>
                    <?php endif;  if(isset($shop_instagram)): ?>
                        <div class="social__item info__social--instagram">
                            <a href="https://www.instagram.com/<?=$shop_instagram?>/">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                    <?php endif;  if(isset($shop_viber)): ?>
                        <div class="social__item info__social--viber">
                            <a href="https://msng.link/vi/<?=$shop_viber?>">
                                <i class="fab fa-viber"></i>
                            </a>
                        </div>
                    <?php endif;  if(isset($shop_whatsapp)): ?>
                        <div class="social__item info__social--whatsapp">
                            <a href="https://api.whatsapp.com/send?phone=<?=$shop_whatsapp?>">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid header__content__container">
        <div class="container">
            <div class="row header__content">
                <div class="header__logo">
                    <a href="/">LIGHTSHOP</a>
                </div>
                <form class="header__search search__block" method="post" action="/search/">
                    <input class="search--string" type="text" name="search" placeholder="Что вы хотите найти?">
                    <!--<input class="search--btn" type="submit" value="Найти">-->
                    <span class="search--content"></span>
                </form>
                <div class="header__cart">
                    <a href="/cart/">
                        <i class="fas fa-shopping-cart"></i>
                        Корзина
                        (<span id="cart-count"><?=Cart::countItems();?></span>)
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid header__menu__container">
        <div class="container">
            <div class="row header__menu">
                <ul class="header__nav__menu">
                    <li class="nav_menu_container">
                        <a href="/">Главная</a>
                    </li>
                    <li class="nav_menu_container drop--menu">
                        <a href="#" onclick="return false">
                            Каталог товаров
                            <span class="drop--element"></span>
                            <ul class="nav_menu_block">
                                    <li class="nav_menu_item">
                                    <a href="/catalog/page:1">
                                      Все товары
                                    </a>
                                </li>
                                <?php foreach ($catalog as $key=>$value): ?>
                                    <li class="nav_menu_item">
                                        <a href="/catalog/<?=$catalog[$key]->alias;?>/page:1">
                                            <?=$catalog[$key]->name;?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </a>
                    </li>
                    <li class="nav_menu_container">
                        <a href="/about/">О компании</a>
                    </li>
                    <li class="nav_menu_container">
                        <a href="/delivery/">Доставка</a>
                    </li>
                    <li class="nav_menu_container">
                        <a href="/contact/">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
<section class="container-fluid content__container">
    <?php require_once $content;?>
</section>
<footer class="container-fluid footer__container">
    <div class="container">
        <div class="row">
            <div class="footer__copy">
                &copy; Lightshop 2019
            </div>
        </div>
    </div>
</footer>
<!--Slider-->
<script type="text/javascript" src="/template/slider/slider.js"></script>
</body>
</html>
