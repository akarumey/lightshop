<div class='container'>
    <?php if(!empty($_SESSION['products'])):?>
    <form method="post" action="/cart/order/">
        <?php
        $model = new \classes\core\Generator;
        $model->gridGenerator($grid_cart);
        ?>
        <div class="row justify-content-between">
            <div><span>Итого: <?=$sum_to_pay;?> Грн</span></div>
            <div><input class="BTN added" type="submit" name="submit" value="Оформить заказ"></div>
        </div>
    </form>
    <?php else:?>
        <div class="row">
            <span>В вашей корзине нет товаров</span>
        </div>
    <?php endif;?>
</div>