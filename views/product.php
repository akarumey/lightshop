<?php if($product->discount != 0){
    $sail = $product->price - ($product->price/100*$product->discount)." Грн";
}?>

<div class='container'>
    <div class='row'>
        <div class="col-md-5"><div class="product__img"><img src="/images/<?php echo $product->image?>" alt=""></div></div>
        <div class="col-md-7">
            <div class="row"><h1><?php echo ucfirst($product->name)?></h1></div>
            <div class="row"><span>ID: <?php echo $product->id?></span></div>
            <div class="row"><p><b>Краткое описание:</b><br><?php echo $product->short_description?></p></div>
            <div class="row">
                <h2>
                    <b>Цена: </b>
                    <span <?php if($product->discount != 0){echo "style='text-decoration: line-through;'";}?>>
                        <?=$product->price?> грн
                    </span>  &nbsp;
                </h2>
                <h2>
                    <span style="color: red">
                        <?=$sail;?>
                    </span>
                </h2>
            </div>
            <div class="row">
                <div class="BTN add-to-cart and-bay" data-id="<?php echo $product->id?>">Купить в один клик</div>
                <div class="BTN add-to-cart" data-id="<?php echo $product->id?>">Добавить в корзину</div>
                <div class="BTN added hide">Добавлено в корзину</div>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>Описание товара:</h2>
        <p><?php echo $product->description?></p>
    </div>
</div>