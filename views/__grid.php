<div class="grid__container">

    <div class="grid__content" style="padding-bottom: <?=$padding;?>">

        <?php if($params['title']['display']==true):?>
            <div class="grid__title justify-content-between">
                <div class="grid__name">
                    <div class="h4"><?=$title;?></div>
                    <span class="grid__decor"> </span>
                </div>
                <?php if($params['title_link']['display']==true):?>
                    <div class="grid__link">
                        <a href="<?=$link;?>"><?=$link_name;?></a>
                    </div>
                <?php endif;?>
            </div>
        <?php endif;?>

        <div class="grid__content__container slider" id="<?=$slider_id?>">

            <?php if($params['slider']['display']==true){
                echo "<div class=\"slider__wrapper\" style='width: 100%'>";
            }?>

            <?php foreach ($content as $key=>$value): ?>

                <div class="grid__content__item__container slider__item" style="width: <?=$width_column?>">
                    <div class="grid__content__item">
                        <?php if($params['marker']['display']==true && $content[$key]->discount != 0):?>
                            <span class="grid__content__marker" style="background-color: <?=$marker_bg?>; color: <?=$marker_color?>;">
                                <?= $content[$key]->discount;?>%
                            </span>
                        <?php endif;?>

                        <?php if($params['delete']['display']==true):?>
                            <a class="grid__content__btn--remove" href="/cart/delete/<?=$content[$key]->id?>" style=" background-color: <?=$delete_color;?>">
                                remove
                            </a>
                        <?php endif;?>

                        <div class="grid__content__img">
                            <img src="/images/<?= $content[$key]->image;?>" alt="">
                        </div>

                        <?php if($content[$key]->status == 0):?>
                            <p class="disabled">Товар закончился</p>
                        <?php endif;?>

                        <?php if($params['count']['display']==true):?>
                            <div class="grid__content__count">
                                <span>Кол-во: </span>
                                <div class="grid__content__input">
                                    <input type="number" name="count_<?=$content[$key]->id?>" value="<?=$item_count[$content[$key]->id]?>">
                                </div>
                                <span> шт.</span>
                            </div>
                        <?php endif;?>

                        <div class="grid__content__name">
                            <?= $content[$key]->name;?>
                        </div>

                        <div class="grid__content__price">
                            <?= $content[$key]->price;?> Грн
                        </div>

                        <?php if($params['button']['display']==true):?>
                            <div class="grid__content__btn">
                                <a href="/product/<?= $content[$key]->id;?>" class="BTN white">Подробнее</a>
                            </div>
                        <?php endif;?>

                    </div>

                </div>

            <?php endforeach; ?>

            <?php if($params['slider']['display']==true){
                echo "</div>";
                echo "<!-- КНОПКИ \"НАЗАД\" И \"ВПЕРЁД\" -->";
                echo "<a class=\"slider__control slider__control_left slider__control_show\" href=\"#\" role=\"button\"></a>";
                echo "<a class=\"slider__control slider__control_right slider__control_show\" href=\"#\" role=\"button\"></a>";
            }?>

        </div>
    </div>
</div>