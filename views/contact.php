<div class="container">
    <div class="row" style="margin-bottom: 20px"><h2>Контакты</h2></div>
    <div class="row __contact">
            <?php if(isset($shop_phone)): ?>
                <div class="social__item info__social--phone">
                    <a class="callback__item info__callback--phone" href="tel:<?=$shop_phone?>">
                        <i class="fas fa-phone"></i><?=$shop_phone?>
                    </a>
                </div>
            <?php endif;  if(isset($shop_email)): ?>
                <div class="social__item info__social--email">
                    <a class="callback__item info__callback--email" href="mailto:<?=$shop_email?>">
                        <i class="fas fa-envelope"></i><?=$shop_email?>
                    </a>
                </div>
            <?php endif; ?>

            <?php if(isset($shop_telegram)): ?>
                <div class="social__item info__social--telegram">
                    <a href="https://t.me/<?=$shop_telegram?>">
                        <i class="fab fa-telegram"></i>
                        <?=$shop_telegram?>
                    </a>
                </div>
            <?php endif;  if(isset($shop_instagram)): ?>
                <div class="social__item info__social--instagram">
                    <a href="https://www.instagram.com/<?=$shop_instagram?>/">
                        <i class="fab fa-instagram"></i>
                        <?=$shop_instagram?>
                    </a>
                </div>
            <?php endif;  if(isset($shop_viber)): ?>
                <div class="social__item info__social--viber">
                    <a href="https://msng.link/vi/<?=$shop_viber?>">
                        <i class="fab fa-viber"></i>
                        <?=$shop_viber?>
                    </a>
                </div>
            <?php endif;  if(isset($shop_whatsapp)): ?>
                <div class="social__item info__social--whatsapp">
                    <a href="https://api.whatsapp.com/send?phone=<?=$shop_whatsapp?>">
                        <i class="fab fa-whatsapp"></i>
                        <?=$shop_viber?>
                    </a>
                </div>
            <?php endif; ?>
    </div>
    <div class="row">
        <p>Украина, г.Днепр, ул.Гоголя 15.</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4449.9054667835935!2d35.05025482224525!3d48.45787987725044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbe2d9a46d6ee3%3A0x6772b458db38ac5a!2z0YPQuy4g0JPQvtCz0L7Qu9GPLCAxNSwg0JTQvdC40L_RgNC-LCDQlNC90LXQv9GA0L7Qv9C10YLRgNC-0LLRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNDkwMDA!5e0!3m2!1sru!2sua!4v1560779566788!5m2!1sru!2sua" width="100%" height="450" frameborder="0" style="border:0; margin-bottom: 40px" allowfullscreen></iframe>
    </div>
</div>