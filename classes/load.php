<?php
/**
 * Рекурсивно сканирует указанную директорию и подключает найденные PHP файлы
 * @param $dir
 */
function includeClasses($dir)
{
    foreach (scandir($dir) as $item) {
        if ($item === '.' || $item === '..' || $item === basename(__FILE__)) continue;

        if (is_dir("$dir/$item")) {
            includeClasses("$dir/$item");
        } else {
            if (pathinfo("$dir/$item", PATHINFO_EXTENSION) === 'php') {
                include "$dir/$item";
            }
        }
    }
}

includeClasses(__DIR__);