<?php
namespace classes\shop;

use classes\core\Model;

/**
 * Class Users
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property int $privilege
 */
class Users extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public static function rules()
    {
        return [
            'id' => ['type' => 'integer'],
            'login' => ['type' => 'string', 'required' => true],
            'password' => ['type' => 'string', 'required' => true],
            'privilege' => ['type' => 'integer'],
        ];
    }
}