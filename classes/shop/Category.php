<?php
namespace classes\shop;

use classes\core\Model;

/**
 * Class Category
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public static function rules()
    {
        return [
            'id' => ['type' => 'integer'],
            'parent_id' => ['type' => 'integer'],
            'name' => ['type' => 'string', 'required' => true],
            'alias' => ['type' => 'string'],
            'created_at' => ['type' => 'integer'],
            'updated_at' => ['type' => 'integer'],
        ];
    }
}