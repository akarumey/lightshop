<?php
namespace classes\shop;

use classes\core\Model;

/**
 * Class Ordercart
 *
 * @property int $id
 * @property int $order_number
 * @property string $product_id
 * @property string $count
 * @property string $created_at
 * @property string $updated_at
 */
class Order_cart extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public static function rules()
    {
        return [
            'id' => ['type' => 'integer'],
            'order_number' => ['type' => 'integer', 'required' => true],
            'product_id' => ['type' => 'integer', 'required' => true],
            'count' => ['type' => 'integer', 'required' => true],
            'created_at' => ['type' => 'integer'],
            'updated_at' => ['type' => 'integer'],
        ];
    }
}