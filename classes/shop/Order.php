<?php
namespace classes\shop;

use classes\core\Model;

/**
 * Class Order
 *
 * @property int $id
 * @property int $email
 * @property string $phone
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $delivery
 * @property string $sum
 * @property string $order_number
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public static function rules()
    {
        return [
            'id' => ['type' => 'integer'],
            'email' => ['type' => 'string', 'required' => true],
            'phone' => ['type' => 'string', 'required' => true],
            'first_name' => ['type' => 'string', 'required' => true],
            'last_name' => ['type' => 'string', 'required' => true],
            'address' => ['type' => 'string'],
            'delivery' => ['type' => 'string', 'required' => true],
            'sum' => ['type' => 'integer', 'required' => true],
            'order_number' => ['type' => 'string'],
            'created_at' => ['type' => 'integer'],
            'updated_at' => ['type' => 'integer'],
        ];
    }
}