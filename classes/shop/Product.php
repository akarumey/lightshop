<?php
namespace classes\shop;

use classes\core\Model;

/**
 * Class Product
 * @property int $id
 * @property int $category
 * @property string $name
 * @property string $image
 * @property string $price
 * @property string $short_description
 * @property string $description
 * @property string $count
 * @property string $discount_status
 * @property string $discount
 * @property string $status
 * @property string $created_at
 * @property string $updated_atp
 */
class Product extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public static function rules()
    {
        return [
            'id' => ['type' => 'integer'],
            'category' => ['type' => 'string', 'required' => true],
            'name' => ['type' => 'string', 'required' => true],
            'image' => ['type' => 'string', 'required' => true],
            'price' => ['type' => 'integer', 'required' => true],
            'short_description' => ['type' => 'string','required' => true],
            'description' => ['type' => 'string','required' => true],
            'count' => ['type' => 'integer'],
            'discount_status' => ['type' => 'string'],
            'discount' => ['type' => 'integer'],
            'status' => ['type' => 'integer'],
            'created_at' => ['type' => 'integer'],
            'updated_at' => ['type' => 'integer'],
        ];
    }
}