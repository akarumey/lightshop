<?php
namespace classes\controllers;

use classes\core\Cart;
use classes\core\LoadContent;
use classes\shop\Product;
use classes\shop\Order;
use classes\shop\Order_cart;

class CartController{

    public function IndexAction(){
        if(!empty($_SESSION['products'])){
            $model = new Product;
            $productInCart = $_SESSION['products'];
            $products = [];

            foreach ($productInCart as $key => $count){
                $product = $model::find()->Where("`id` = '$key'")->one();
                array_push($products, $product);
            };
        }

        $grid_cart = [
            'title'=>['display'=>true, 'name'=>'Корзина'],
            'count'=>['display'=>true,],
            'delete'=>['display'=>true, 'color'=> 'red'],
            'button'=>['display'=>false],
            'content'=>$products,
        ];

        $sum_to_pay = Cart::countSum();

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/cart.php";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function AddAction($id){

        $model = new Cart;
        echo $model::addProduct($id);
        return true;

    }

    public function DeleteAction($id){

        unset($_SESSION['products'][$id]);

        $referer = $_SERVER['HTTP_REFERER'];
        header("Location:".$referer);

    }

    public function OrderAction(){
        $model = new Product;
        $productInCart = $_SESSION['products'];
        $products = [];

        foreach ($productInCart as $key => $count){
            $product = $model::find()->Where("`id` = '$key'")->one();
            array_push($products, $product);

            if($count != $_POST['count'.$product->id]){
                $productInCart[$key] = $_POST["count_".$key];
            }
        };
        $_SESSION['products'] = $productInCart;

        $sum_to_pay = Cart::countSum();

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/order.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function Reg_orderAction(){

        $order_array = [
          'time' => time(),
          'sum' => $_POST['sum'],
            'delivery' => $_POST['delivery']
        ];

        if(empty($_POST['email'])){
            $massage = ['status' => 'error', 'text' => 'Вы не указали email'];
        }else{
            $order_array['email'] = $_POST['email'];
        }
        if(empty($_POST['phone'])){
            $massage = ['status' => 'error', 'text' => 'Вы не указали телефон'];
        }else{
            $order_array['phone'] = $_POST['phone'];
        }
        if(empty($_POST['first_name'])){
            $massage = ['status' => 'error', 'text' => 'Вы не указали имя'];
        }else{
            $order_array['first_name'] = $_POST['first_name'];
        }
        if(empty($_POST['last_name'])){
            $massage = ['status' => 'error', 'text' => 'Вы не указали фамилию'];
        }else{
            $order_array['last_name'] = $_POST['last_name'];
        }

        if($_POST['delivery'] != "stock"){
            if(empty($_POST['city'])){
                $massage = ['status' => 'error', 'text' => 'Вы не указали город'];
            }else{
                $city = $_POST['city'];
            }
            if(empty($_POST['street'])){
                $massage = ['status' => 'error', 'text' => 'Вы не указали улицу'];
            }else{
                $street = $_POST['street'];
            }
            if(empty($_POST['house'])){
                $massage = ['status' => 'error', 'text' => 'Вы не указали номер дома'];
            }else{
                $house = $_POST['house'];
            }

            $apartment = $_POST['apartment'];

            //Собираем адрес в строку
            $address = $city.", ул. ".$street.", дом. ".$house.",";
            if(!empty($apartment)){$address = $address." кв. ".$apartment;}

            $order_array['address'] = $address;
        }

        $productInCart = $_SESSION['products'];
        if(empty($massage)){
            $model = new LoadContent;
            $order_number = $model::addOrder($order_array, $productInCart);
            $_SESSION['order'] = [
              'order_number' => $order_number,
                'sum' => $order_array['sum'],
            ];
            $massage = ['status' => 'link', 'text' => '/cart/success/'];
        }

        echo json_encode($massage);
    }

    public function SuccessAction(){
        $order = $_SESSION['order'];
        unset($_SESSION['order_number']);
        unset($_SESSION['products']);

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/success.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }
}