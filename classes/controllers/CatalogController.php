<?php
namespace classes\controllers;

use classes\shop\Product;

/**
 * Class CatalogController
 * @package classes\controllers
 */
class CatalogController{

    public function AllAction($page, $category=null ){

        //Настройки отображения
        $limit = 8; //Товаров на странице
        $offset = $limit*($page-1); //Смещение в зависимости от страницы
        $column = 4; //Количество столбцов с товарами
        $width_column = 100/$column."%"; //Нужная ширина столбца

        //Сортировка
        if(!empty($_POST['sort'])){
            $_SESSION['sort'] = $_POST['sort'];
            $_SESSION['category'] = $category;
        }
        if ($_SESSION['sort'] == 'sort_up' && $_SESSION['category'] == $category){
            $sort='`price`';
        } elseif ($_SESSION['sort'] == 'sort_down' && $_SESSION['category'] == $category) {
            $sort = '`price` DESC';
        } else {
            $sort = '`id`';
            unset($_SESSION['sort']);
        }

        //Магия)
        $model = new Product;
        $result = $model::pagination()->getContent(true, $page, $limit, $offset, $category, $sort);

        foreach ($result['pages'] as $key => $value)
        {
            foreach ($value as $key => $value){
                $pagination[$key] = $value;
            }
        }
        $product = $result['data'];
        $first = $result['first'];
        $last = $result['last'];
        $count = $result['count'];

        $grid_catalog=[
            'column'=>4,
            'content'=>$product,
        ];

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/catalog.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function DiscountAction(){
        $column = 5;
        $width_column = 100/$column."%";

        //Магия)
        $model = new Product;
        $product = $model::find()->Where(["and", "`status` = '1'", "`discount_status` = 'true'"])->all();

        $grid_discount=[
            'column' => 5,
            'content' => $product
        ];

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/discount.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }
}