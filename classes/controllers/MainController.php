<?php
namespace classes\controllers;

use classes\shop\Product;

class MainController{

    public function IndexAction(){
        $slides = require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/slides.php";

        //Настройки отображения
        $discount_limit = 5; //Товаров со скидкой
        $new_limit=10; //Новых товаров

        //Магия)
        $model = new Product;

        $discount = $model::find()->Where(["and", "`status` = '1'", "`discount_status` = 'true'"])->Options("LIMIT $discount_limit")->all();

        $product = $model::find()->Where("`status` = '1'")->Options("ORDER by `id` DESC LIMIT $new_limit")->all();

        $grid_discount=[
            'title'=>['display'=>true, 'name'=>'Скидки'],
            'title_link'=>['display'=>true, 'name'=>'Все товары', 'link'=>'/catalog/discount'],
            'content'=>$discount,
        ];

        $grid_new=[
            'padding'=>'30px',
            'slider'=>['display'=>true, 'id'=>'product-slider'],
            'title'=>['display'=>true, 'name'=>'Новинки'],
            'title_link'=>['display'=>true, 'name'=>'Открыть каталог', 'link'=>'/catalog/page:1'],
            'content'=>$product,
        ];

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/index.php";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function ProductAction($product_id){

        $model = new Product;
        $product=$model::find()->Where("`id`=$product_id")->one();

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/product.php";

        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function SearchingAction(){

        if(empty($_POST['search'])){
            header("Location: /");
        }
        $string = $_POST['search'];

        $model = new Product();
        $product = $model::find()->Where("`name` LIKE '%$string%'")->Options("LIMIT 10")->all();
        $find_content =[];
        $attr = $model::attributes();

        foreach ($product as $k => $v){
            foreach ($attr as $value){
                $find_content[$k][] = [$value => $v->$value];
            }
        }
        echo json_encode($find_content);
    }

    public function SearchAction(){
        $string = $_POST['search'];
        $model = new Product();
        $product = $model::find()->Where("`name` LIKE '%$string%'")->all();

        $grid_search =[
            'column' => 5,
            'content'=> $product
        ];

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/search.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function AboutAction(){
        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/about.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function DeliveryAction(){
        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/delivery.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }

    public function ContactAction(){
        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/contact.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/layouts/default.php";
    }


}