<?php

namespace classes\controllers;

use classes\core\LoadContent;
use classes\shop\Category;
use classes\shop\Order_cart;
use classes\shop\Product;
use classes\shop\Users;

class AdminController{

    public function IndexAction(){
       header("Location: /admin/order/page:1");
    }

    public function ViewsAction($param, $page){
        //Настройки отображения
        $limit = 20; //Товаров на странице
        $offset = $limit*($page-1); //Смещение в зависимости от страницы

        $className = 'classes\shop\\'.ucfirst($param);
        $model = new $className;

        $titles=$model::attributes();
        //$items = $model::find()->all();
        $result = $model::pagination()->getContent(false, $page, $limit, $offset);

        foreach ($result['pages'] as $key => $value)
        {
            foreach ($value as $key => $value){
                $pagination[$key] = $value;
            }
        }
        $items = $result['data'];
        $first = $result['first'];
        $last = $result['last'];
        $count = $result['count'];

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/$param.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/layouts/default.php";
    }

    public function ItemAction($param, $id){
        $className = 'classes\shop\\'.ucfirst($param);
        $model = new $className;
        $titles=$model::attributes();
        $item = $model::find()->Where("`id`='$id'")->one();

        if (isset($_POST['submit'])) {
            $content = $_POST;
            $model = new LoadContent;
            if ($_POST['table'] == 'category') {
                $model::category($className, $content, $id);
                header("Location: /admin/$param/page:1");
            } elseif ($_POST['table'] == 'product'){
                if($_FILES['image']['size'] > 0){
                    $old_files = $item->image;
                    $new_files = $_FILES;
                    $file_name = $model::updateFile($old_files, $new_files);
                    $content['image'] = $file_name;
                }
                $model::product($className, $content, $id);
                header("Location: /admin/$param/page:1");
            }
        }

        if($param == "product") {
            $model = new Category;
            $categoryes = $model::find()->all();
        }else if($param == "order"){
            $order_number = $item->order_number;

            $productInCart =[];
            $model = new Order_cart();
            $cart = $model::find()->Where("`order_number` = '$order_number'")->all();

            foreach ($cart as $value){
                $product_id = $value->product_id;
                $count[$product_id] = $value->count;

                $product = $model = new Product($product_id);
                array_push($productInCart, $product);
            }
        }

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/edit_$param.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/layouts/default.php";
    }

    public function CreateAction($param)
    {
        $className = 'classes\shop\\' . ucfirst($param);

        if($param == "product") {
            $model = new Category;
            $categoryes = $model::find()->all();
        }

        if (isset($_POST['submit'])) {
            $content = $_POST;
            $model = new LoadContent;
            if ($_POST['table'] == 'category') {
                $model::category($className, $content);
                header("Location: /admin/$param/page:1");
            } elseif ($_POST['table'] == 'product'){
                $new_files = $_FILES;
                $file_name = $model::updateFile(null, $new_files);
                $content['image'] = $file_name;
                $model::product($className, $content);
                header("Location: /admin/$param/page:1");
            }
        }

        $model = new $className;
        $titles=$model::attributes();

        $content = $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/edit_$param.php";
        require_once $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/layouts/default.php";
    }

    public function LoginAction(){

        $login = '';
        $password = '';

        if(isset($_POST['submit'])){
            $login = $_POST['login'];
            $password = md5(md5(trim($_POST['password'])));

            if(!empty($login)){
                $model = new Users;

                if(($model::find()->Where("`login` = '$login'")->one()->password) == $password){
                    session_start();
                    $_SESSION['admin_id'] = ($model::find()->Where("`login` = '$login'")->one()->id);
                    header("Location: /admin/");
                } else {
                    echo 'Не верный логин или пароль';
                }
            } else {
                echo 'Не верный логин или пароль';
            }

        }

        require_once "" . $_SERVER['DOCUMENT_ROOT'] . "/../views/admin/login.php";

    }

    public function LogoutAction(){
        session_start();
        session_destroy();
        unset($_SESSION['admin_id']);
        if(!$_SESSION['admin_id']){header("Location: /admin/login");}
    }
}
