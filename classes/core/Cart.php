<?php
namespace classes\core;

use classes\shop\Product;

/**
 * Class Ordercart
 */
class Cart
{
   public static function addProduct($id){
       $id = intval($id);
       $productsInCart = [];

       if(isset($_SESSION['products'])){
           $productsInCart = $_SESSION['products'];
       }

       if (array_key_exists($id, $productsInCart)){
           $productsInCart[$id]++;
       } else {
           $productsInCart[$id] = 1;
       }

       $_SESSION['products'] = $productsInCart;

       return self::countItems();
   }

   public static function countItems(){
       if (isset($_SESSION['products'])){
           $count = 0;

           foreach ($_SESSION['products'] as $id => $quantity){
               $count = $count + $quantity;
           }
           return $count;
       } else {
           return 0;
       }
   }

   public static function countSum(){

       $productsInCart = $_SESSION['products'];
       $sum = 0;

       $model = new Product;

       foreach ($productsInCart as $key => $value){
           $price = $model::find()->Where("`id`='$key'")->one();

           if($price->discount != 0){
               $sum = $sum + ($value*($price->price - ($price->price/100*$price->discount)));
           } else {
               $sum = $sum + ($value*($price->price));
           }
       }

       return $sum;
   }
}