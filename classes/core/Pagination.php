<?php
namespace classes\core;

/**
 * Class Pagination
 * @package classes\core
 */
class Pagination{

    protected static $modelClass;
    protected static $table;

    public function __construct($modelClass)
    {
        self::$modelClass = $modelClass;
    }

    public function content_from_db_catalog($limit, $offset, $category, $sort)
    {

        $model = new static::$modelClass;

        if(!empty($category)){
            return $model::find()->Where("`category`= '$category'")->Options("ORDER by `status` DESC, $sort LIMIT $limit OFFSET $offset")->all();
        } else {
            return $model::find()->Options("ORDER by `status` DESC, $sort LIMIT $limit OFFSET $offset")->all();
        }

    }

    public function content_from_db($limit, $offset, $category, $sort){
        $model = new static::$modelClass;
        return $model::find()->Options("ORDER by `id` DESC LIMIT $limit OFFSET $offset")->all();
    }

    public function countPage($limit, $category){

        $model = new static::$modelClass;

        if(!empty($category)){
            $count = $model::find()->Select("COUNT(`id`)")->Where("`category`= '$category'")->count();
        } else {
            $count = $model::find()->Select("COUNT(`id`)")->count();
        }

        $page = ceil($count/$limit);

        return $page;
    }

    public function pages($page, $count, $category){

        if(!empty($category)){
            $category = $category."/";
        }

        $pages = [];

        if($page<=3){

            if($count > 5){
                $count = 5;
            }

            for ($i=1;$i<=$count;$i++){
                $item = ['link'=>$category."page:".$i, 'num'=> $i];
                array_push($pages, $item);
            }

        } elseif($page+2 < $count) {

            for ($i=$page-2;$i<=$page+2;$i++){
                $item = ['link'=>$category."page:".$i, 'num'=> $i];
                array_push($pages, $item);
            }

        } else {
            for ($i=$count-4;$i<=$count;$i++){
                $item = ['link'=>$category."page:".$i, 'num'=> $i];
                array_push($pages, $item);
            }
        }

        return [
          'pages' => $pages,
        ];
    }

    public function getContent($catalog, $page, $limit, $offset, $category, $sort){
        if($catalog == true){
            $data = $this->content_from_db_catalog($limit, $offset, $category, $sort);
        } else {
            $data = $this->content_from_db($limit, $offset, $category, $sort);
        }


        $count = $this->countPage($limit, $category);

        if(!empty($category)){
            $first = $category."/page:1";
            $last = $category."/page:".$count;
        } else {
            $first = "page:1";
            $last = "page:".$count;
        }

        $pages = $this->pages($page, $count, $category);


        return [
            "data" => $data,
            "pages" => $pages,
            "first" => $first,
            "last" => $last,
            "count" => $count,
        ];
    }
}
