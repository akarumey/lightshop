<?php
namespace classes\core;

/**
 * Class Model
 *
 * @property int $id
 * @property array $errors
 */
class Model
{
    private $db;
    private $currentData;
    private $updatedData;
    public $errors;

    public static $test;

    /**
     * Model constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->db = new DB();

        if (!empty($id)) {
            $tableName = static::tableName();

            $query = $this->db->query("SELECT * FROM `$tableName` WHERE `id` = '$id'");
            $this->currentData = $query->fetch();
        }
    }

    /**
     * Получает значение атрибута модели
     *
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (!static::attributeExists($name)) {
            throw new \Exception("Attribute \"$name\" is not defined.");
        }

        return $this->currentData[$name] ?? null;
    }

    /**
     * Устанавливает значение атрибута модели
     *
     * @param string $name
     * @param mixed $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if (!static::attributeExists($name)) {
            throw new \Exception("Attribute \"$name\" is not defined.");
        }

        $this->currentData[$name] = $value;
        $this->updatedData[$name] = $value;
    }

    /**
     * Создает экземпляр класса Query для дальнейшей работы с запросом
     * @return Query
     */
    public static function find()
    {
        return new Query(static::class);
    }

    public static function pagination()
    {
        return new Pagination(static::class);
    }

    /**
     * Название таблицы модели
     * @return string
     */
    public static function tableName()
    {
        return null;
    }

    /**
     * Массив правил валидации атрибутов модели
     * @return array
     */
    public static function rules()
    {
        return [];
    }

    /**
     * Массив доступных атрибутов модели
     * @return array
     */
    public static function attributes()
    {
        return array_keys(static::rules());
    }

    /**
     * @inheritdoc
     */
    public static function attributeExists($attribute)
    {
        return key_exists($attribute, static::rules());
    }

    /**
     * @param array $data
     */
    public function load(array $data)
    {
        foreach ($data as $attribute => $value) {
            if (static::attributeExists($attribute)) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        foreach (static::rules() as $attribute => $rules) {
            if ($this->currentData[$attribute] !== null && isset($rules['type'])) {
                if (!$this->validateType($this->currentData[$attribute], $rules['type'])) {
                    $this->errors[$attribute][] = 'Incorrect value type.';
                }
            }

            if (($rules['required'] ?? false) && empty($this->currentData[$attribute])) {
                $this->errors[$attribute][] = "Attribute $attribute required.";
            }
        }

        return empty($this->errors);
    }

    /**
     * @param string $value
     * @param string $type
     * @return bool
     */
    protected function validateType($value, $type)
    {
        $oldValue = $value;
        $oldType = gettype($oldValue);
        if ($oldType === $type) return true;

        settype($value, $type);
        settype($value, $oldType);

        return $oldValue === $value;
    }

    /**
     * @param bool $validate
     * @return bool
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        $insert = true;

        if (!empty($this->__get('id'))) {
            $tableName = static::tableName();
            $query = $this->db->query("SELECT `id` FROM `$tableName` WHERE `id` = '$this->id'");

            if ($query->rowCount() > 0) {
                $insert = false;
            }
        }

        $tableName = static::tableName();

        if ($insert) {
            $insertKeys = [];
            $insertValues = [];
            foreach (static::attributes() as $attribute) {
                $insertKeys[] = "`$attribute`";
                $insertValues[$attribute] = $this->$attribute === null ? 'NULL' : "'".$this->$attribute."'";
            }

            if (key_exists('created_at', $insertValues) && isset($insertValues['created_at'])) {
                $insertValues['created_at'] = "'".time()."'";
            }
            if (key_exists('updated_at', $insertValues) && isset($insertValues['updated_at'])) {
                $insertValues['updated_at'] = "'".time()."'";
            }

            $sql = "INSERT INTO `$tableName` (".implode(',', $insertKeys).") VALUES (".implode(",", $insertValues).")";

            return $this->db->exec($sql) !== false;
        } elseif (!empty($this->updatedData)) {
            if (static::attributeExists('updated_at')) {
                $this->updated_at = time();
            }

            $updateData = [];
            foreach ($this->updatedData as $attribute => $value) {
                $updateData[] = "`$attribute`=".($value === null ? 'NULL' : "'".$value."'");
            }

            $sql = "UPDATE `$tableName` SET ".implode(',', $updateData)." WHERE `id`='".$this->id."'";

            return $this->db->exec($sql) !== false;
        }

        return true;
    }
}