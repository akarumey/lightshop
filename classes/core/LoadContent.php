<?php

namespace classes\core;

use classes\shop\Order;
use classes\shop\Order_cart;

class LoadContent{

    public static function addOrder($arr, $productInCart){

        $model = new Order();
        $model->email = $arr['email'];
        $model->phone = $arr['phone'];
        $model->first_name = $arr['first_name'];
        $model->last_name = $arr['last_name'];
        if(!empty($arr['address'])){
            $model->address = $arr['address'];
        }
        $model->delivery = $arr['delivery'];
        $model->sum = $arr['sum'];
        $model->save();

        $email = $arr['email'];
        $phone = $arr['phone'];

        $order = $model::find()->Where(['and', "`email` = '$email'", "`phone` = '$phone'"])->Options("ORDER by `id` DESC LIMIT 1")->one();


        $order_number = $arr['time'] . $order->id;

        $model = new Order($order->id);
        $model->order_number = $order_number;
        $model->save();

        static::addToCart($productInCart, $order_number);
        return $order_number;
    }

    public static function addToCart($arr, $order_number){

        foreach ($arr as $key => $count) {
            $model = new Order_cart();
            $model->order_number = $order_number;
            $model->product_id = $key;
            $model->count = $count;
            $model->save();
        };
    }

    public static function updateFile($old_files, $new_files){
        if(!empty($old_files)){
            $file = $_SERVER['DOCUMENT_ROOT'] ."/images/".$old_files;
            unlink($file);
        }

        $token = time();

        $image_tmp_name = $new_files['image']["tmp_name"];
        $image_type = end(explode('.', $new_files['image']['name']));
        $image_name = $new_files['image']['new_name'] = $token.'.'.$image_type;
        $image_dir = 'images/'; // Директория для фотографий товаров

        $error_flag = $new_files['image']["error"];

        // Если ошибок не было
        if($error_flag == 0) {
            if(!move_uploaded_file($image_tmp_name, $image_dir . $image_name)){
                return "error";
            }
        }

        return $image_name;
    }

    public static function product($className,$content, $id=null){
        if(!empty($id)){$model = new $className($id);}
        else {$model = new $className;}

        if(!empty($content['category'])){$model->category=$content['category'];}
        if(!empty($content['name'])){$model->name=$content['name'];}
        if(!empty($content['price'])){ $model->price=$content['price'];}
        if(!empty($content['image'])){$model->image=$content['image'];}
        if(!empty($content['short_description'])){
            $model->short_description=$content['short_description'];
        }
        if(!empty($content['description'])){$model->description=$content['description'];}
        if(!empty($content['count'])){$model->count=$content['count'];}
        if(!empty($content['discount_status'])){$model->discount_status=$content['discount_status'];}
        if(!empty($content['discount'])){
            $model->discount=$content['discount'];
        } else {
            $model->discount=0;
        }
        if(!empty($content['status'])){$model->status=$content['status'];}
        $model->save();

        return $model->errors;
    }

    public static function category($className, $content, $id=null){
        if(!empty($id)){$model = new $className($id);}
        else {$model = new $className;}

        if(!empty($content['parent_id'])){$model->parent_id=$content['parent_id'];}
        if(!empty($content['name'])){$model->name=$content['name'];}
        if(!empty($content['alias'])){$model->alias=$content['alias'];}
        $model->save();

        return $model->errors;
    }

}