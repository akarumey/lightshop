<?php
namespace classes\core;

/**
 * Class Query
 * @package classes\core
 */
class Query
{
    private $db;
    protected $sql;
    protected $conditions;
    protected $options;
    protected $select;

    protected static $modelClass;
    protected static $table;

    /**
     * Query constructor.
     * @param Model $modelClass
     */
    public function __construct($modelClass)
    {
        $this->db = new DB();

        self::$modelClass = $modelClass;
        self::$table = $modelClass::tableName();
    }

    /**
     * Создает запрос из всех заданных условий
     */
    protected function createQuery()
    {
        if(!empty($this->select)){
            $this->sql = "SELECT $this->select FROM `" . self::$table . "`";

        } else {
            $this->sql = "SELECT * FROM `" . self::$table . "`";
        }

        if(!empty($this->conditions)){
            $this->sql .= "WHERE" . $this->conditions;
        }
        if(!empty($this->options)){
            $this->sql .= $this->options;
        }
    }

    /**
     * @param $arr
     * @return string
     */
    protected function convertArr($arr){
        for ($i=0; $i<count($arr); $i++) {
            if (is_array($arr[$i]))
                $arr[$i] = $this->convertArr($arr[$i]);
        }
        $glue = " ".strtoupper($arr[0])." ";
        $arr = array_slice($arr, 1);
        return $this->conditions = "(".implode($glue, $arr).")";
    }

    /**
     * @param $arr
     * @return $this
     */
    public function Where($arr)
    {
        if (is_array($arr)) {
            $this->convertArr($arr);
        } else {
            $this->conditions = "(".$arr.")";
        }

        return $this;
    }

    public function Options($arr){
        $this->options = $arr;

        return $this;
    }

    public function Select($arr){
        $this->select = $arr;

        return $this;
    }

    /**
     * @return Model[]
     */
    public function all()
    {
        // @todo: проверить

        $this->createQuery();

        $models = [];
        foreach ($this->db->query($this->sql)->fetchAll() as $row) {
            $model = new self::$modelClass();
            $model->load($row);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @return Model
     */
    public function one()
    {
        // @todo: проверить

        $this->createQuery();

        $row = $this->db->query($this->sql)->fetch();

        $model = new self::$modelClass();
        $model->load($row);

        return $model;

    }

    public function count()
    {
        // @todo: проверить

        $this->createQuery();

        $row = $this->db->query($this->sql)->fetchAll();

        return $row['0']['0'];
    }
}