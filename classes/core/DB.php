<?php
namespace classes\core;

/**
 * Class DB
 * @package classes\core
 */
class DB extends \PDO
{
    /**
     * DB constructor.
     */
    public function __construct()
    {
        $db = Config::get('db');

        try {
            parent::__construct('mysql:host='.$db['host'].';dbname='.$db['database'].';port='.$db['port'].';charset='.$db['charset'], $db['username'], $db['password']);
        } catch (\Exception $exception) {
            exit($exception->getMessage());
        }
    }
}