<?php

namespace classes\core;

class Generator{

    protected $grid_default = [
        'column'=>5,
        'padding'=>'10px',
        'slider'=>['display'=>false, 'id'=>''],
        'title'=>['display'=>false, 'name'=>'test'],
        'title_link'=>['display'=>false, 'name'=>'', 'link'=>''], //Только вместе с заголовком
        'count'=>['display'=>false],
        'marker'=>['display'=>true, 'bg'=>'red', 'color'=>'white'],
        'delete'=>['display'=>false, 'color'=>''],
        'button'=>['display'=>true],
        'content'=>[],
    ];

    /**
     * @param array $params
     */
    public function gridGenerator(array $params=null){
        $params = array_merge($this->grid_default, $params);

        $slider_id=$params['slider']['id'];
        $width_column = 100/$params['column']."%";
        $padding=$params['padding'];
        $title=$params['title']['name'];
        $link_name = $params['title_link']['name'];
        $link=$params['title_link']['link'];
        $marker_bg=$params['marker']['bg'];
        $marker_color=$params['marker']['color'];
        $delete_color=$params['delete']['color'];
        $item_count=$_SESSION['products'];

        $content = $params['content'];

        return require $_SERVER['DOCUMENT_ROOT']."/../views/__grid.php";

    }
}