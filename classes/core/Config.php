<?php
namespace classes\core;

/**
 * Class Config
 * @package classes\core
 */
class Config
{
    private static $data;

    /**
     * Подключает массив настроек из файла настроек
     * @return bool
     */
    private static function loadFile()
    {
        $file = __DIR__.'/../../config/main.php';
        if (!file_exists($file)) {
            return false;
        }

        self::$data = require_once __DIR__.'/../../config/main.php';

        return true;
    }

    /**
     * Получает набор значений (секцию настроек $section) или конкретное значение из секции по ключу $variable
     * @param string $section
     * @param string $variable
     * @return array|null
     */
    public static function get($section, $variable = null)
    {
        if (empty(self::$data)) {
            self::loadFile();
        }

        if ($variable === null) {
            //return isset(self::$data[$section]) && isset(self::$data[$section]) !== false ? isset(self::$data[$section]) : null;
            return self::$data[$section] ?? [];
        }

        return self::$data[$section][$variable] ?? null;
    }
}