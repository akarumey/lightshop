<?php

namespace classes\core;

/**
 * Class Router
 * @package classes\core
 */
Class Router{

    protected $routes = [];
    protected $params = [];
    protected $get_query = [];

    /**
     * Router constructor.
     */
    public function __construct(){

        $routes = Config::get('routes');
        foreach ($routes as $route => $params){
            $this->routes[$route] = $params;
        }
    }

    /**
     * @return string
     */
    private function getURL(){
        if (!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run(){
        //Получить строку запроса
        $url = $this->getURL();

        //Проверяем на совпадение
        foreach ($this->routes as $urlPattern => $path){
            if(preg_match("~$urlPattern~", $url)){
                $internalRoute = preg_replace("~$urlPattern~", $path, $url);
                $segment = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segment).'Controller');
                $actionName = ucfirst(array_shift($segment).'Action');
                $parameters = $segment;

                break;
            }
        }

        //Подгружаем нужную страницу
        $controllerFile = 'classes\controllers\\'.$controllerName;

        if(class_exists($controllerFile)){
            $controllerObject = new $controllerFile;

            if(method_exists($controllerObject, $actionName)){
                return call_user_func_array([$controllerObject, $actionName], $parameters);
            } else {
                header("Location: /");
            }
        }
    }

}